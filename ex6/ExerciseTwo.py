from functools import wraps

memo = {}


def memoization(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        sign = hash(*args, **kwargs)
        if sign in memo.keys():
            return memo[sign]
        val = func(*args, **kwargs)
        memo[hash(*args, **kwargs)] = val
        return val

    return wrapper


def logging(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        print('Call %s with params %s' % (func, *args))
        return func(*args, **kwargs)
    return wrapper


def stack_trace(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper()
