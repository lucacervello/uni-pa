class Person:
    def __init__(self, name, lastname, birthday):
        self.name = name
        self.lastname = lastname
        self.birthday = birthday

    def __repr__(self):
        print('name : %s, lastname : %s, birthday : %s' % (self.name, self.lastname, self.birthday))


class Student(Person):
    def __init__(self, name, lastname, birthday):
        super(Student, self).__init__(name, lastname, birthday)
        self.lectures = {}

    def _calculate_average(self):
        if len(self.lectures) == 0:
            return 0
        else:
            return sum([self.lectures[key] for key in self.lectures.keys()]) / len(self.lectures)

    grade_average = property(fget=_calculate_average)


class Worker(Person):
    def __init__(self, name, lastname, birthday, pay_per_hour):
        super(Worker, self).__init__(name, lastname, birthday)
        self.pay_per_hour = pay_per_hour

    def _get_day_salary(self):
        return self.pay_per_hour * 8

    def _get_week_salary(self):
        return self._get_day_salary() * 5

    def _get_month_salary(self):
        return self._get_week_salary() * 4

    def _get_year_salary(self):
        return self._get_month_salary() * 12

    day_salary = property(fget=_get_day_salary)
    week_salary = property(fget=_get_week_salary)
    month_salary = property(fget=_get_month_salary)
    year_salary = property(fget=_get_year_salary)

# import time
#
# class Wizard(Person):
#     age = property(fget=_get_age, fset=_set_age)
#
#     def _get_age(self):
#         return time.time() - super().birthday
#
#     def _set_age(self, age):
#         super().birthday = 2

