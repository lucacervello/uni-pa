from ex6.ExerciseOne import Worker
from functools import wraps
from types import FunctionType


def only_double(func):
    calls = 1

    @wraps(func)
    def wrapper(*args, **kwargs):
        nonlocal calls
        print('calls #%s' % calls)
        if calls % 2 == 0:
            calls += 1
            return func(*args, **kwargs)
        else:
            calls += 1
            return None
    return wrapper


class Counter(type):

    def __new__(mcs, classname, supers, classdict):
        for attr, attrval in classdict.items():
            if type(attrval) is FunctionType:
                classdict[attr] = only_double(attrval)
        return super(Counter, mcs).__new__(mcs, classname, supers, classdict)

    def __init__(cls, name, bases, attrs):
        super(Counter, cls).__init__(name, bases, attrs)


class Spell(type):

    def __new__(cls, *args, **kwargs):
        return Worker


class PersonTest:

    def __init__(self, name, lastname, birthday):
        self.name = name
        self.lastname = lastname
        self.birthday = birthday

    @only_double
    def say_hi(self):
        return 'hi %s' % self.name


class Person(metaclass=Counter):

    def __init__(self, name, lastname, birthday):
        self.name = name
        self.lastname = lastname
        self.birthday = birthday

    def __repr__(self):
        return 'name : %s, lastname : %s, birthday : %s' % (self.name, self.lastname, self.birthday)

    def say_hi(self):
        return 'hi %s' % self.name


p = Person('luca', 'cervello', '1/3/1994')
