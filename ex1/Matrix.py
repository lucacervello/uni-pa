
def entity(size):
    mat = matrix(size)
    for i in mat:
        for j in mat:
            if j == i:
                mat[i][j] = 1
            else:
                mat[i][j] = 0
    return mat


def matrix(n):
    mat = list()
    for i in list(range(n)):
        mat.append(row(n))
    return mat


def row(n):
    r = list()
    for i in list(range(n)):
        r.append(0)
    return r

