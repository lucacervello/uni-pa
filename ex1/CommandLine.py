import os


def cat(file_list):
    for file in file_list:
        with open(file, 'r') as f:
            print(f.read())


def chmod(file_list, cmd):
    for file in file_list:
        os.chmod(file, cmd)
