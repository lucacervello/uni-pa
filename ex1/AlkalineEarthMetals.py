alkaline_earth_metals = [
    ['barium', 56],
    ['berillyum', 4],
    ['calcium', 20],
    ['magnesium', 12],
    ['radium', 88],
    ['strontium', 38]
]

noble_gases = {
    'helium': 2,
    'neon': 10,
    'argon': 18,
    'kripton': 36,
    'xenon': 54,
    'radon': 86
}


def max_aem():
    return max([value for _, value in alkaline_earth_metals])


def sort():
    return sorted(alkaline_earth_metals, key=lambda x: x[1])


def to_dict():
    dic = {}
    for key, value in alkaline_earth_metals:
        dic[key] = value
    return dic


def merge():
    result = {}
    for k, v in alkaline_earth_metals:
        result[k] = v
    for k, v in noble_gases.items():
        result[k] = v
    return result
