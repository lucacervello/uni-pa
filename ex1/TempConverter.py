
class Scale:
    Kelvin = 'kelvin'
    Rankine = 'rankine'
    Delisle = 'delisle'
    Newton = 'newton'
    Reaumur = 'reaumur'
    Romer = 'romer'
    Fahrenheit = 'fahrenheit'
    Celsius = 'celsius'
    all_scales = [Fahrenheit, Kelvin, Rankine, Delisle, Newton, Reaumur, Romer, Celsius]


def table(temp):
    result = ''
    for scale in Scale.all_scales:
        result += to_all(temp, scale)
    return result


def to_all(temp, scale, sort=False):
    string = ''
    results = map(lambda x: [x, celsius_to(to_celsius(temp, scale), x)], filter(lambda x: x != scale, Scale.all_scales))
    if sort:
        results = sorted(results, key=lambda x: x[1])

    for to_scale, t in results:
        string += 'from ' + scale + ': ' + str(temp) + ' to ' + to_scale + ': ' + str(t) + '\n'

    return string


def celsius_to(temp, scale):
    if scale == Scale.Celsius:
        return temp
    elif scale == Scale.Kelvin:
        return temp + 273.15
    elif scale == Scale.Rankine:
        return (temp + 273.15) * (9/5)
    elif scale == Scale.Delisle:
        return (100 - temp) * (3/2)
    elif scale == Scale.Newton:
        return temp * (33/100)
    elif scale == Scale.Reaumur:
        return temp * (4/5)
    elif scale == Scale.Romer:
        return temp * 21/40 + 7.5
    elif scale == Scale.Fahrenheit:
        return temp * (9/5) + 32
    else:
        raise AttributeError


def to_celsius(temp, scale):
    if scale == Scale.Celsius:
        return temp
    elif scale == Scale.Kelvin:
        return temp - 273.15
    elif scale == Scale.Rankine:
        return (temp - 491.67) * (5/9)
    elif scale == Scale.Delisle:
        return 100 - temp * (2/3)
    elif scale == Scale.Newton:
        return temp * (100/33)
    elif scale == Scale.Reaumur:
        return temp * (5/4)
    elif scale == Scale.Romer:
        return (temp - 7.5) * (40/21)
    elif scale == Scale.Fahrenheit:
        return (temp - 32) * 5/9
    else:
        raise AttributeError
