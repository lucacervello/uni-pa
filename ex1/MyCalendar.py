import calendar as cal


def is_leap(year):
    return cal.isleap(year)


def leap_count(list_year):
    return len([x for x in list_year if cal.isleap(x)])


def day_of_week(day, month, year):
    return cal.weekday(year, month, day)
