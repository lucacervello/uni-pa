import calendar as cal
import unittest
import os

from ex1 import AlkalineEarthMetals as Aem, MyCalendar as MyCal
from ex1 import TempConverter as Tc
from ex1.TempConverter import Scale
from ex1 import Matrix as Mx
from ex1 import CommandLine as Cl


class CalendarTest(unittest.TestCase):

    def test_cal(self):
        self.assertEqual(MyCal.is_leap(2017), cal.isleap(2017))

    def test_leaf_count(self):
        self.assertEqual(MyCal.leap_count(range(2000, 2051)), cal.leapdays(2000, 2051))

    def test_weekday(self):
        self.assertEqual(MyCal.day_of_week(26, 6, 2016), cal.weekday(2016, 6, 26))


class AlcalineEarthMetalsTest(unittest.TestCase):

    alkaline_earth_metals_sorted = [
        ['berillyum', 4],
        ['magnesium', 12],
        ['calcium', 20],
        ['strontium', 38],
        ['barium', 56],
        ['radium', 88]
    ]

    alkaline_earth_metals_dictionary = {
        'barium': 56,
        'berillyum': 4,
        'calcium': 20,
        'magnesium': 12,
        'radium': 88,
        'strontium': 38
    }

    noble_gases = {
        'helium': 2,
        'neon': 10,
        'argon': 18,
        'kripton': 36,
        'xenon': 54,
        'radon': 86
    }

    merged_dict = {
        'barium': 56,
        'berillyum': 4,
        'calcium': 20,
        'magnesium': 12,
        'radium': 88,
        'strontium': 38,
        'helium': 2,
        'neon': 10,
        'argon': 18,
        'kripton': 36,
        'xenon': 54,
        'radon': 86
    }

    def test_max(self):
        self.assertEqual(Aem.max_aem(), 88)

    def test_sort(self):
        self.assertEqual(Aem.sort(), self.alkaline_earth_metals_sorted)

    def test_to_dict(self):
        self.assertEqual(Aem.to_dict(), self.alkaline_earth_metals_dictionary)

    def test_merge(self):
        self.assertEqual(Aem.merge(), self.merged_dict)


class TempConverterTest(unittest.TestCase):

    result = 'from celsius: 500 to fahrenheit: 932.0\n' \
             'from celsius: 500 to kelvin: 773.15\n' \
             'from celsius: 500 to rankine: 1391.67\n' \
             'from celsius: 500 to delisle: -600.0\n' \
             'from celsius: 500 to newton: 165.0\n' \
             'from celsius: 500 to reaumur: 400.0\n' \
             'from celsius: 500 to romer: 270.0\n'

    sorted_result = 'from celsius: 500 to delisle: -600.0\n' \
                    'from celsius: 500 to newton: 165.0\n' \
                    'from celsius: 500 to romer: 270.0\n' \
                    'from celsius: 500 to reaumur: 400.0\n' \
                    'from celsius: 500 to kelvin: 773.15\n' \
                    'from celsius: 500 to fahrenheit: 932.0\n' \
                    'from celsius: 500 to rankine: 1391.67\n' \


    def test_to_all(self):
        self.assertEqual(self.result, Tc.to_all(500, Scale.Celsius))

    def test_to_all_sorted(self):
        self.assertEqual(self.sorted_result, Tc.to_all(500, Scale.Celsius, sort=True))


class MatrixTest(unittest.TestCase):
    result_identity = [[1, 0],
                       [0, 1]]

    def identity(self):
        self.assertEqual(self.result_identity, Mx.entity(2))


@unittest.SkipTest
class CommandLineTest(unittest.TestCase):

    @staticmethod
    def test_cat():
        Cl.cat(['hello.txt'])

    def test_chmod(self):
        with open('hello.txt', 'r') as f:
            writable = f.writable()
            Cl.chmod(['hello.txt'], 2222)
            self.assertNotEqual(writable, f.writable())

    def tearDown(self):
        os.chmod('hello.txt', 7777)


if __name__ == '__main__':
    unittest.main()
