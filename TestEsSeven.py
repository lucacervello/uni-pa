import unittest
from ex7.ExerciseTwo import goldbach, goldbach_list


class TestExerciseTwo(unittest.TestCase):
    def test_four(self):
        self.assertEqual((2, 2), goldbach(4))

    def test_ten(self):
        self.assertEqual((3, 7), goldbach(10))

    def test_16(self):
        self.assertEqual((5, 11), goldbach(16))

    def test_six(self):
        self.assertEqual((3, 3), goldbach(6))

    def test_8(self):
        self.assertEqual((3, 5), goldbach(8))

    def test_12(self):
        self.assertEqual((5, 7), goldbach(12))

    def test_14(self):
        self.assertEqual((3, 11), goldbach(14))

    def test_18(self):
        self.assertEqual((7, 11), goldbach(18))

    def test_20(self):
        self.assertEqual((7, 13), goldbach(20))


if __name__ == '__main__':
    unittest.main()