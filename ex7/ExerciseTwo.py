def goldbach(n):
    if n % 2 == 0 and n is not 2:
        for a in prime_range(2, n):
            for b in prime_range(2, n):
                if a + b == n:
                    return a, b


def prime_range(mi, ma):
    for x in range(mi, ma):
        if is_prime(x):
            yield x


def is_prime(n):
    return [x for x in range(2, n) if n % x == 0] == []


def goldbach_list(n, m):
    pass