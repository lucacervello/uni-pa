
class Matrix:

    def __init__(self, matrix=[], n=0, m=0):
        self._matrix = matrix
        self._n = n
        self._m = m

    def __eq__(self, other):
        if self._n != other._n:
            return False
        if self._m != self._m:
            return False
        for i in range(self._n):
            for j in range(self._m):
                if self[i][j] != other[i][j]:
                    return False
        return True

    def __len__(self):
        return self._n

    def __getitem__(self, item):
        return self._matrix[item]

    def __copy__(self):
        return 'hello'

if __name__ == '__main__':
    m = Matrix()
    m1 = Matrix([[1]], 1, 1)
    m2 = Matrix([[2]], 1, 1)
    print(m == m1)
    print(m1 == m2)
    print(m1 == m1)
    m2 = m1
    print(m1)
    print(m2)