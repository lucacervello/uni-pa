import re


class KeWord:
    def __init__(self, filename):
        with open(filename, mode='r') as file:
            lines = file.readlines()
        lines = [re.findall(r'\w+', line) for line in lines if line != '\n']
        lines_formatted = [' '.join(line) for line in lines]
        print(lines_formatted)
        lines = [map(lambda x: x.lower(), line) for line in lines]
        lines = [list(filter(lambda x: len(x) > 2 and x != 'the' and x != 'and', line)) for line in lines]
        keys = []
        for line in lines:
            keys += line
        keys = sorted(keys)
        keys = KeWord.zip_keys(keys, lines_formatted)
        for (line, index, index_line) in keys:
            print('{} {}'.format(index_line, KeWord.fill_left(line, index)))

    @staticmethod
    def zip_keys(keys, lines):
        for key in keys:
            for line in lines:
                if key in line.lower():
                    yield (line, line.lower().index(key), lines.index(line) + 1)

    @staticmethod
    def fill_left(line, index):
        return '{} {}'.format(' ' * (40 - index), line)


if __name__ == '__main__':
    a = KeWord('testo.txt')