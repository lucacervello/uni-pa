import re
from datetime import date, timedelta
from functools import wraps

class Person:
    def __init__(self, name, lastname, birthday):
        self.name = name
        self.lastname = lastname
        (day, month, year) = re.match(r'^(\d{1,2})/(\d{1,2})/(\d{2,4})', birthday).groups()
        self.birthday = date(int(year), int(month), int(day))

    def __repr__(self):
        return self.__dict__.__str__()


class Student(Person):
    def __init__(self, name, lastname, birthday):
        super().__init__(name, lastname, birthday)
        self.lectures = {}
        self._grade_average = 0

    @property
    def grade_average(self):
        if len(self.lectures) != 0:
            for val in self.lectures.values():
                self._grade_average += val
            return self._grade_average / len(self.lectures)
        else:
            return 0


class Worker(Person):
    def __init__(self, name, lastname, birthday, pay_per_hour):
        super(Worker, self).__init__(name, lastname, birthday)
        self.pay_per_hour = pay_per_hour

    @property
    def day_salary(self):
        return self.pay_per_hour * 8

    @property
    def week_salary(self):
        return self.day_salary * 5

    @property
    def month_salary(self):
        return self.week_salary * 4

    @property
    def year_salary(self):
        return self.month_salary * 12


class Wizard(Person):

    @property
    def age(self):
        return (self.birthday - date.today()).days

    @age.setter
    def age(self, age):
        self.birthday = (date.today() - timedelta(days=age))

# ------------------------------------------------------------------


def memoization(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        self = args[0]
        if 'memo' in self.__dict__.keys():
            memo = self.__dict__['memo']
        else:
            self.__dict__['memo'] = {}
            memo = self.__dict__['memo']

        if hash(args) in memo.keys():
            return memo[hash(args)]
        else:
            res = func(*args, **kwargs)
            memo[hash(args)] = res
            return res
    return wrapper


def logging(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        print('function name : {} with args: {}'.format(func.__name__, args))
        return func(*args, **kwargs)
    return wrapper


def stack_trace(func):
    import inspect

    @wraps(func)
    def wrapper(*args, **kwargs):
        frame = inspect.currentframe()
        while frame is not None:
            print('{} with args: {}'.format(frame.f_code.co_name, args))
            frame = frame.f_back
        return func(*args, **kwargs)
    return wrapper


class Numeric:

    @memoization
    @logging
    def mem_fib(self, n):
        if n > 1:
            return self.mem_fib(n-1) + self.mem_fib(n - 2)
        else:
            return n

    @stack_trace
    def fib(self, n):
        if n > 1:
            return self.fib(n-1) + self.fib(n - 2)
        else:
            return n


@stack_trace
def hello():
    print('hello')


def counter_inc(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        Counter.count += 1
        return func(*args, **kwargs)
    return wrapper


class Counter(type):

    count = 0

    def __init__(cls, name, supers, clsdict):
        print(clsdict)
        clsdict['__init__'] = counter_inc(clsdict['__init__'])
        super(Counter, cls).__init__(name, supers, clsdict)


class CounterPerson(Person, metaclass=Counter):
    def __init__(self, name, lastname, birthday):
        super().__init__(name, lastname, birthday)

if __name__ == '__main__':
    p = Person('luca', 'cervello', '1/3/1994')
    print(p)
    s = Student('luca', 'cervello', '1/3/1994')
    s.lectures['pa'] = 30
    print(s)
    print(s.grade_average)
    w = Worker('luca', 'cervello', '1/3/1994', 10)
    print(w.month_salary)
    n = Numeric()
    n.mem_fib(10)
    n.fib(10)
    cp = CounterPerson('uca', 'cervlo', '1/3/1993',)
    cp1 = CounterPerson('luca', 'cervello', '1/3/1994',)
    print(CounterPerson.count)
