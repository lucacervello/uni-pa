

def fact(n):
    res = 1
    for x in range(1, n + 1):
        res *= x
    return res


def only_odd(n):
    i = 0
    x = 1
    while n > i:
        if x % 2 != 0:
            yield x
        x += 1
        i += 1


def sin(y, n):
    res = 0
    sign = 1
    for x in only_odd(n):
        res = res + (sign * ((y ** x) / fact(x)))
        if sign == 1:
            sign = -1
        else:
            sign = 1
    return res
