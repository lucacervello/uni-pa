
def infinite_range():
    i = 1
    while True:
        yield i
        i += 1


def sum_all_natural(n):
    return sum(filter(lambda x: x % 3 == 0 and x % 5 == 0, range(0, n)), 0)


def divisible_number(n):
    def fn(x):
        return all(map(lambda y: x % y == 0, range(1, n + 1)))
    for i in infinite_range():
        if fn(i):
            return i


def sum_two_at_1k():
    return sum(map(int, str(2 ** 1000)), 0)


def fib(n):
    a, b = 0, 1
    while a < n:
        yield a
        a, b = b, a + b


def first_fibo_1000():
    for i in infinite_range():
        print(i)
        if len(str(fib(i))) == 1000:
            return i
