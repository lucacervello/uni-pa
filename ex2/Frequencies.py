

def freqs(filename, number):
    with open(filename, 'r') as filename:
        text = filename.read()
        return frequencies_with_min(text, number)


def frequencies(text):
    dic = {}
    text = [x for x in text if x.isalnum()]
    for l in text:
        if l in dic.keys():
            dic[l] += 1
        else:
            dic[l] = 1
    return dic


def frequencies_with_min(text, min):
    return {k: v for k, v in frequencies(text).items() if v >= min}
    # return dict(filter(lambda v: v > min, frequencies(text).values()))
