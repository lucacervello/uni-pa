import inspect


class AutoInit(type):

    def __call__(self, *args, **kwargs):
        method_info = inspect.getfullargspec(self.__init__)

        for key, val in zip(method_info.args[1:], args):
            setattr(self, key, val)

        return self


class Test(metaclass=AutoInit):
    def __init__(self, x, y):
        pass


test = Test(1, 2)
print(test.x)