import re


def translate(file):
    with open(file, 'r') as file:
        lines = file.readlines()
        lines_translated = [_translate_line(line) for line in lines]
        lines_translated.append('</html>')
        lines_translated.insert(0, '<html>')
        #return lines_translated
        return ''.join(lines_translated)


def _translate_line(line):
    if line.startswith('# '):
        return '<h1>' + _format_line(line[2:]) + '</h1>'
    elif line.startswith('## '):
        return '<h2>' + _format_line(line[2:]) + '</h2>'
    elif line.startswith('### '):
        return '<h3>' + _format_line(line[3:]) + '</h3>'
    elif line.startswith('* '):
        return '<li>' + _format_line(line[1:]) + '</li>'
    elif line.strip() == '---':
        return '<hr />'
    elif line.strip() != '':
        return '<p>' + _format_line(line) + '</p>'
    else:
        return ''


def _format_line(line):
    bold_regex = re.compile(r'\*\*\w*\*\*')

    line = _format(bold_regex, line, 2, '<strong>')

    italic_regex = re.compile(r'_\w*_')

    line = _format(italic_regex, line, 1, '<em>')

    code_regex = re.compile(r'`\w*`')

    line = _format(code_regex, line, 1, '<code>')

    return line


def _format(regex, text, md_tag_length, html_tag):
    tag_text = re.findall(regex, text)
    html_tag_end = html_tag.replace('<', '</')
    for tag in tag_text:
        text = re.sub(regex, '%s%s%s' % (html_tag, tag[md_tag_length: -md_tag_length], html_tag_end), text)
    return text
