import unittest
from exam.ruffini.Polynomial import Polynomial, Monomial


class TestPolinomial(unittest.TestCase):

    def test_init(self):
        pol = Polynomial(1, 2)
        self.assertEqual({0: 2, 1: 1}, pol.dict_pol)

    def test_add(self):
        pol1 = Polynomial(1, 2, 3)
        pol2 = Polynomial(2, 3, 4)
        pol_res = Polynomial(3, 5, 7)
        self.assertEqual((pol1 + pol2).dict_pol, pol_res.dict_pol)

    def test_sub(self):
        pol1 = Polynomial(1, 2, 3)
        pol2 = Polynomial(2, 3, 1)
        pol_res = Polynomial(-1, -1, 2)
        self.assertEqual((pol1 - pol2).dict_pol, pol_res.dict_pol)

    def test_lshift(self):
        pol1 = Polynomial(1, 2, 3)
        mon = Monomial(10, 1)
        pol_res = Polynomial(11, 12, 13, 0)
        self.assertEqual((pol1 << mon).dict_pol, pol_res.dict_pol)


if __name__ == '__main__':
    unittest.main()
