class ExtendedString(str):

    def is_palindrome(self) -> bool:
        str_iter = [x for x in self.lower() if x.isalnum()]
        return "".join(str_iter) == "".join(reversed(str_iter))

    def __sub__(self, other):
        return ExtendedString("".join([x for x in self if x not in other]))
