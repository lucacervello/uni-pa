from math import pi


class EquilateralTriangle:
    def __init__(self, side):
        self.__side = side

    def calculate_area(self):
        return ((self.__side ** 2) * (3 ** 1/2))/4

    def calculate_perimeter(self):
        return self.__side * 3


class Circle:
    def __init__(self, radius):
        self.__radius = radius

    def calculate_area(self):
        return self.__radius ** 2 * pi

    def calculate_perimeter(self):
        return self.__radius * 2 * pi


class Rectangle:
    def __init__(self, width, lenght):
        self.__width = width
        self.__lenght = lenght

    def calculate_area(self):
        return self.__lenght * self.__width

    def calculate_perimeter(self):
        return self.__lenght * 2 + self.__width * 2


class Square:
    def __init__(self, side):
        self.__side = side

    def calculate_area(self):
        return self.__side ** 2

    def calculate_perimeter(self):
        return self.__side * 4


class Pentagon:
    def __init__(self, side):
        self.__side = side

    def calculate_area(self):
        return 1.720 * self.__side ** 2

    def calculate_perimeter(self):
        return self.__side * 5