class SortedDict(dict):
    def __init__(self, *args, **kwargs):
        super(SortedDict, self).__init__(self, *args, **kwargs)
        self.keys = list(super(SortedDict, self).keys())

    def keys(self):
        return self.keys

    def __setitem__(self, key, value):
        super(SortedDict, self).__setitem__(key, value)
        self.keys = sorted(self.keys + [key])

    def __delitem__(self, key):
        super(SortedDict, self).__delitem__(key)
        self.keys.remove(key)

    def clear(self):
        super(SortedDict, self).clear()
        self.keys.clear()

    def __iter__(self):
        return iter((k, self[k]) for k in self.keys)

    def values(self):
        return [super(SortedDict, self).get(k) for k in self.keys]