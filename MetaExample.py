
class AutoInit(type):

    def __init__(cls, *args, **kwargs):
        print(args)
        print(kwargs)
        super.__init__(cls, args, kwargs)

    def __new__(cls, *args, **kwargs):
        print(args)
        print(kwargs)
        super.__new__(cls, args, kwargs)

    def __call__(cls, *args, **kwargs):
        print(*args)
        print(kwargs)


class Person(metaclass=AutoInit):
    pass

p = Person('luca', 'cervello')
print(p)