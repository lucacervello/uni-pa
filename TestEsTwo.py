import unittest
import math
from ex2 import MathList as Ml, Frequencies as F, Trigonometric as T


class MathListTest(unittest.TestCase):

    def test_sum_natural(self):
        self.assertEqual(Ml.sum_all_natural(1000), 33165)

    def test_divisible_number(self):
        self.assertEqual(Ml.divisible_number(10), 2520)

    def test_sum_two_at_1k(self):
        self.assertEqual(Ml.sum_two_at_1k(), 1366)


class FrequenciesTest(unittest.TestCase):

    def test_frequencies(self):
        self.assertEqual({"a": 2, "b": 1}, F.frequencies("aab"))

    def test_frequencies_with_min(self):
        self.assertEqual({"a": 3, "b": 2}, F.frequencies_with_min("aaabbc", 2))


class TrigonometricTest(unittest.TestCase):
    def test_sin(self):
        self.assertAlmostEqual(T.sin(10, 100), math.sin(10), delta=0.0000000000001)


if __name__ == '__main__':
    unittest.main()