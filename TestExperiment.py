import unittest
from experiment.meta import Meta


class TestLog(unittest.TestCase):
    @unittest.skip
    def test_hello(self):
        log = Meta.MetaLog()
        self.assertEqual("hello", log.hello)


if __name__ == '__main__':
    unittest.main()