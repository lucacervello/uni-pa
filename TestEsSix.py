import unittest
from ex6.ExerciseOne import Person, Student, Worker
from ex6.ExerciseThree import Person as PersonCounter


class TestExerciseOne(unittest.TestCase):
    def setUp(self):
        self.person = Person('luca', 'cervello', '01/03/1994')
        self.student = Student('luca', 'cervello', '01/03/1994')
        self.worker = Worker('luca', 'cervello', '01/03/1994', 12)

    def test_person(self):
        self.assertEqual(self.person.name, 'luca')
        self.assertEqual(self.person.lastname, 'cervello')
        self.assertEqual(self.person.birthday, '01/03/1994')

    def test_student(self):
        self.assertEqual(self.student.grade_average, 0)
        self.student.lectures["mate"] = 26
        self.assertEqual(self.student.grade_average, 26)
        self.student.lectures["eng"] = 24
        self.assertEqual(self.student.grade_average, 25)

    def test_worker(self):
        self.assertEqual(self.worker.day_salary, 12 * 8)
        self.assertEqual(self.worker.week_salary, 12 * 5 * 8)
        self.assertEqual(self.worker.month_salary, 12 * 5 * 8 * 4)
        self.assertEqual(self.worker.year_salary, 12 * 5 * 8 * 4 * 12)


class TestExerciseThree(unittest.TestCase):
    def test_person_counter(self):
        student = PersonCounter(name='luca', lastname='cervello', birthday='12/12/2012')
        student1 = PersonCounter('luca', 'cervello', '12/12/12')
        student2 = PersonCounter('luca', 'cervello', '12/12/12')
        self.assertEqual(PersonCounter.times, 3)


if __name__ == '__main__':
    unittest.main()