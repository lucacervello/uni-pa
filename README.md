# Esercizi Esame Programmazione Avanzata unimi

## SERIE 1

## Esercizio 1

Exercise 1: The Calendar Module.

In the following exercises, you will work with Python's calendar module:

    Visit the Python documentation website at http://docs.python.org/3.1/modindex.html, and look at the documentation on the calendar module.
    Import the calendar module.
    Read the description of the function isleap(). Use isleap() to determine the next leap year.
    Find and use a function in the module calendar to determine how many leap years there will be between years 2000 and 2050, inclusive.
    Find and use a function in module calendar to determine which day of the week July 29, 2016 will be.

Exercise 2: Alkaline Earth Metals.

Assign a list that contains the atomic numbers and the names of the six alkaline earth metals---barium (56), beryllium (4), calcium (20), magnesium (12), radium (88), and strontium (38)---to a variable called alkaline_earth_metals.

    Write a one-liner that returns the highest atomic number in alkaline_earth_metals.
    Using one of the list methods, sort alkaline_earth_metals in ascending order (from the lightest to the heaviest).
    Transform the alkaline_earth_metals into a dictionary using the name of the metals as the dictionary's key.
    Create a second dictionary containing the noble gases -- helium (2), neon (10), argon (18), krypton (36), xenon (54), and radon (86) -- and store it in the variable noble_gases.
    Merge the two dictionaries and print the result as couples (name, atomic number) sorted in ascending order on the element names.

Note that Python's dictionaries neither preserve the insertion order nor are sorted in some way.
Exercise 3: Temperature Conversion System.

Beyond the well-known Celsius and Fahrenheit, there are other six temperature scales: Kelvin, Rankine, Delisle, Newton, Réaumur, and Rømer (Look at http://en.wikipedia.org/wiki/Comparison_of_temperature_scales to read about them).

    Write a function (table) that given a pure number returns a conversion table for it (as a string) among any of the 8 temperature scales (remember that functions are objects as well).
    Write a function (toAll) that given a temperature in a specified scale returns a string for all the corresponding temperatures in the other scales, the result must be sorted on the temperatures and the scale must be specified.

Exercise 4: Matrix Calculi.

A matrix can be represented as a list of lists (rows and columns).

    Use the comprehensions to define a function (identity) that returns the identity matrix (the one with all 0s but the 1s on the diagonal) of given size.
    Use the comprehensions to define a function (square) that returns a square matrix filled with the first n*n integers with n given as an argument.
    Write the function transpose to transpose a generic matrix independently of the size and content.
    Write the function multiply to multiply two matrices non necessarily square matrices.

Exercise 5: Shell Commands Simulation.

Similarly to the ls-l example please implement:

    The cat command, i.e., a command that given a list of files prints their content on the terminal (man cat to get more info).
    The chmod command, i.e., a command that permits to change the access mode of a given group of files (man chmod to get more info)
    The more command, i.e., a command that given a file prints its content 30 rows at a time and wait a keystroke every 30 rows before printing the next 30.


## SERIE 2

### Exercise 1: A Few of Math with Lists.

Write the solutions for the following quizzes by using functional programming:

    Sum all the natural numbers below one thousand that are multiples of 3 or 5.
    Calculate the smallest number divisible by each of the numbers 1 to 20.
    Calculate the sum of the figures of 2^1000
    Calculate the first term in the Fibonacci sequence to contain 1000 digits.

### Exercise 2: Frequencies.

Let's write a module (a pool of functions) that given a quite large text (over than 2000 words) counts how frequent each word occurs in the text. In particular the module should provide the function freqs that given a filename and a number would return a list of words (with their frequencies) that occur more than the given number; the list is sorted by frequency with the higher first.

The text is read from a file and it is a real text with punctuation (i.e., commas, semicolons, ...) that shouldn't be counted.

Note that words that differ only for the case should be considered the same.


### Exercise 3: Approximated Trigonometric Functions.

sin(x) can be approximate by the Taylor's series:

x - x^3/3! + x^5/5! ...

Let's write a library to implement sin(x, n) by using the Taylor's series (where n is the level of approximation, i.e., 1 only one item, 2 two items, 3 three items and so on).

Let's compare your function with the one implemented in the math module at the growing of the approximation level.

Hint. Use a generator for the factorial and a comprehension for the series.

## SERIE 3

### Exercise 1: Playing around with Geometry.

To implement the classes representing: equilateral triangles, circles, rectangles, squares and pentagons with the following characteristics/properties/capabilities.

    they should understand the calculate_area() and calculate_perimeter() messages with the obvious meaning
    the state must be private
    a list of geometric shapes must be sortable by area and by perimeter (not at the same time, of course)
    to add an hexagon class should maintain all the capabilities of the existing classes and correctly interact with them
    to write a iterator that permits to return the elements of a list of geometric shapes sorted by increasing areas.

### Exercise 2: Playing around with Algebra.

A monoid is an algebraic structure consisting of a set together with a single associative binary operation and an identity element. E.g., the set of booleans with the or operator is a monoid whose identity is false.

A group is an algebraic structure consisting of a set together with an operation that combines any two of its elements to form a third element. To qualify as a group, the set and the operation must satisfy a few conditions called group axioms, namely closure, associativity, identity and invertibility. E.g., the set Z (the integers with sign) with the + operator is a group but with the * is not since inverting the operation break the closure property.

A ring is an algebraic structure consisting of a set together with two binary operations (usually called addition and multiplication), where each operation combines two elements to form a third element. To qualify as a ring, the set together with its two operations must satisfy certain conditions, namely, the set must be an abelian (i.e., commutative) group under addition and a monoid under multiplication such that multiplication distributes over addition.

Write the Monoid, Group and Ring classes implementing the monoids, groups and rings algebraic structures respectively. Each class must be general, in the sense that the operations and the sets are not a priori defined and they should implement methods to check the properties characterizing the algebraic structure.

Test your implementation with the following examples (S is the set, add=additive operation, mul=multiplicative operation, i=identity):

Monoids

    S = {True, False} add = or i = False
    S = Zn={0,1,...,n-1} add = + where a+b=(a+b)%n i = 0

Groups

    S = itertools.permutations('RGB'), given a a function swaps 1st with 2nd element in the permutation, and b a function that swaps 2nd with 3rd element add = function composition i = a(a(x))
    S = Q-{0} add = * i = 1

Rings

    S = {0} add = + where 0+0=0 mul = * where 0*0=0 i = 0
    S = Z add = + mul = * i = 0
    S = Z4 = {0,1,2,3} add = + where a+b=(a+b)%4 mul = * where a*b=(a*b)%4 i = 0

Note infinite sets can be implemented through iterators; property checks on infinite sets should be on a finite subset.
### Exercise 3: Playing around with Arithmetic (the Polish Calculator).

Write a PolishCalculator class that implements a stack-based calculator that adopts polish notation for the expressions to be evaluated.

Polish Notation is a prefix notation wherein every operator follows all of its operands; this notation has the big advantage of being unambiguous and permits to avoid the use of parenthesis. E.g., (3+4)*5 is equal to 3 4 + 5 *.

An instance of the PolishCalculator class will understand the following API:

    __init(self)__ with the obvious meaning
    __str(self)__ which will format the expression in the corresponding infix notation
    eval(str) which will evaluate the expression contained in the string str and written in the polish notation

The recognized operators are +, - (both unary and binary), *, /, ** over integers and floats, or, and and not over booleans. At least a space ends each operands and operators, T and F respectively represent True and False.

Hint. The evaluation/translation can be realized by pushing the recognized elements on a stack.
### Exercise 4: Playing around with Extensions.

Python's dictionaries do not preserve the order of inserted data nor store the data sorted by the key. Write an extension for the dict class whose instances will keep the data sorted by their key value. Note that the order must be preserved also when new elements are added.


## SERIE 4

### Exercise 1: Playing around with Strings.

To extend the class for the strings to support the following operations:

    to check if the string is palindrome, a string is palindrome when the represented sentence can be read the same way in either directions in spite of spaces, punctual and letter cases, e.g., detartrated, "Do geese see God?", "Rise to vote, sir.", ...
    to subtract the letters in a string from the letters in another string, e.g., "Walter Cazzola"-"abcwxyz" will give "Wlter Col" note that the operator - is case sensitive and that the target should be a name containing an instance of the child class
    given a dictionary of strings, to check if the string is an anagram of one or more of the strings in the dictionary

### Exercise 2: Social Networks.

A social network is a social structure made of individuals (or organizations) called nodes, which are tied (connected) by one or more specific types of interdependency, such as friendship, kinship, financial exchange, dislike, sexual relationships, or relationships of beliefs, knowledge or prestige.

A graph is an abstract representation of a set of objects where some pairs of the objects are connected by links. The interconnected objects are represented by mathematical abstractions called vertices, and the links that connect some pairs of vertices are called edges.

The exercise consists of:

    to implement the social network as a graph, i.e., to define the graph data structure with the operations you consider necessary to implement a social network
    to implement an operation that visits in a convenient way all the elements of the graph, such an operation should be associated to the __str__ operation of the graph implementation
    to test it against a dummy social network.

### Exercise 3: Pascal's Triangle.

Pascal's triangle is a geometric arrangement of the binomial coefficients in a triangle.

The rows of Pascal's triangle are conventionally enumerated starting with row 0, and the numbers in each row are usually staggered relative to the numbers in the adjacent rows. A simple construction of the triangle proceeds in the following manner. On row 0, write only the number 1. Then, to construct the elements of following rows, add the number directly above and to the left with the number directly above and to the right to find the new value. If either the number to the right or left is not present, substitute a zero in its place.

Implement an iterator for the Pascal's triangle that:

    at each step will return another iterator containing all the elements in the corresponding stage sorted as in the triangle
    the iterator can go forward and backward
    (advanced) it is possible to define the working algebra for the triangle, e.g., it could be on Z7 or on the alphabet

## SERIE 5

### Exercise 1: Test Driven Development on Functions.

To write the test units to support the development of the following functions:

    a function anagram that checks if a string passed as a parameter is palindrome, a string is palindrome when the represented sentence can be read the same way in either directions in spite of spaces, punctual and letter cases, e.g., detartrated, "Do geese see God?", "Rise to vote, sir.", ...
    a function validate that checks if a string composed of 'X', 'O' and ' ' is a correct configuration for the tic-tac-toe game and if it is a winning configuration for the 'X' or the 'O' symbols, an even configuration or if there are still moves.
    a function pluralize that transforms a noun from singular to plural in English (the rules and the solution non test-driven are available in the slides).

### Exercise 2: Test Driven Development on Classes.

To write the test units to support the development of the following classes:

    a class SquareMatrix representing the square matrix of given size whose constructor initialize the matrix to the identity and with the methods transpose(), sum_by_row(), sum_by_column() and determinant() with the obvious meanings.
    a class Geography that maps each country with its neighbors when built and with a method neighbors() that took a country's names returns the list of its neighbors.
    a class account similar to that defined in lesson 14 with the following methods deposit(), withdraw(), balance() and the constraint that the balance cannot be negative.

### Exercise 3: Test Driven Development for the Pascal's Triangle.

To write the test units to support the development of the following exercise from the last set.

Pascal's triangle is a geometric arrangement of the binomial coefficients in a triangle.

The rows of Pascal's triangle are conventionally enumerated starting with row 0, and the numbers in each row are usually staggered relative to the numbers in the adjacent rows. A simple construction of the triangle proceeds in the following manner. On row 0, write only the number 1. Then, to construct the elements of following rows, add the number directly above and to the left with the number directly above and to the right to find the new value. If either the number to the right or left is not present, substitute a zero in its place.

Implement an iterator for the Pascal's triangle that:

    at each step will return another iterator containing all the elements in the corresponding stage sorted as in the triangle
    the iterator can go forward and backward
    (advanced) it is possible to define the working algebra for the triangle, e.g., it could be on Z7 or on the alphabet

## SERIE 6

### Exercise 1: Properties, Descriptors and Operator Overloading.

Let us consider a class Person with the following attributes: name, lastname, birthday with the obvious meaning and the corresponding setter and getters and the __repr__ to print it.

    Extend the class Person in the class Student by adding a dictionary lectures with the lecture name as a key and the mark as a value, and the property grade_average to calculate the marks average
    Extend the class Person in the class Worker by adding an attribute pay_per_hour and the properties day_salary, week_salary, month_salary, and year_salary considering 8 working hours a day, 5 working days a week, 4 weeks a month, 12 months a year; note that to set one of the properties implies to recalculate the pay_per_hour value
    Extend the class Person in the class Wizard by adding a property age that when used as a getter calculates the correct age in term of passed days from the birthday to the current day and when used as a setter it will change the birthday accordingly rejuvenating or getting old magically.

Repeat the exercise by using the descriptors instead of the properties.
### Exercise 2: Decorators.

Let us consider a class MyMath with the following methods: fib, fact and taylor implementing the Fibonacci's series, the factorial and the Taylor's series for a generic function and a level of approximation respectively. Then implement the following decorators:

    @memoization applied to a method stores in the class previously calculated results and reuses them instead of recalculating
    @logging applied to a method writes on a file the method name, its actual arguments when a method is called (also by recursion)
    @stack_trace applied to a method prints its stack trace, i.e., the list of calls made to carry out the invocation.

### Exercise 3: Metaclasses.

Let us consider the class Person again and implement the following metaclasses:

    The metaclass Counter which counts how many times Person has been instantiated
    The metaclass Spell that transforms the instances of Person in a Worker with the properties/descriptors of the previous exercise as real methods/attributes
    The metaclass MultiTriggeredMethod that let activate a method only when called twice

## SERIE 7

### Exercise 1: Playing with Files and Strings.

A KeWord In Context (KWIC) index is a simple index for a list of lines or titles. This assignment involves creating a KWIC index for an input list of titles stored in a file. Here's a small example. For the input file:

Casablanca

The Maltese Falcon

The Big Sleep

your program should produce the output:

3                              The Big Sleep    .

1                                  Casablanca   .

2                      The Maltese Falcon       .

2                              The Maltese Falcon

3                          The Big Sleep        .

As you can see, each title is listed for each word (omitting some minor words). The titles are arranged so that the word being indexed is shown in a column on the page. The position the lines have in the input file is shown on the left in the result.

Your Python solution should follow the following rules:

    The input is just a series of titles, one per line. Any leading or trailing spaces should be removed. Internal spaces should be retained (trimmed to one).
    A word is a maximal sequence of non-blank characters.
    The output line is at most 79 characters wide.
        The number is 5 characters wide, right-justified.
        There is a space after the number.
        The key word starts at position 40 (numbering from 1).
        If the part of the title left of the keyword is longer than 33, trim it (on the left) to 33.
        If the part of the keyword and the part to the right is longer than 40, trim it to 40.
    Each title appears in the output once for each word that isn't minor. Any word of length two or less is minor, and and the words are minor words.
    If a title has a repeated word, it should be listed for each repetition.
    Sorting should be case-insensitive.

### Exercise 2: A Few More Math.

Goldbach's conjecture is one of the oldest unsolved problems in number theory and in all of mathematics. It states:

Every even integer greater than 2 is a Goldbach number, i.e., a number that can be expressed as the sum of two primes.

Expressing a given even number as a sum of two primes is called a Goldbach partition of the number. For example,

04 = 2 +  2           6 = 3 +  3           8 = 3 +  5

10 = 7 +  3          12 = 5 +  7          14 = 3 + 11

16 = 5 + 11          18 = 7 + 11          20 = 7 + 13

Write the following Python functions:

    goldbach(n) that returns a Goldbach partition for n
    goldbach_list(n,m) that returns a dictionary indexed on the even numbers in the range (n,m) and whose values are their Goldbach partition

### Exercise 3: A Matrix Class.

You are to write a class Matrix for representing and manipulating integer matrices. An instance of this class stores the elements of a matrix as a list of lists of integers, and provides methods for the operations of matrix equivalence, matrix copy, matrix addition, scalar-matrix multiplication, matrix-matrix multiplication, matrix transposition, and matrix norm -- the "size" of a matrix. Override the appropriate operators and raise the appropriate exceptions.

We first define these operations, and then give a skeleton of the Matrix class showing the signatures for all methods and constructors you must implement.

The operations

Let aij denote the i,j-th element of matrix A, located at row i, column j. Using this notation, the matrix operations listed above may be defined precisely as follows:

    matrix equivalence A ≃ B where A in Zm×n, B in Zp×q when m = p and n = q and bij = aij for i = 1,...,m j = 1,...,n;
    matrix copy B = A where A, B in Zm×n (bij = aij for i = 1,...,m j = 1,...,n);
    matrix addition C = A + B where A, B, C in Zm×n (cij = aij + bij for i = 1,...,m j = 1,...,n);
    scalar-matrix multiplication B = aA where A, B in Zm×n, a in Z (bij = a·aij for i = 1,...,m j = 1,...,n);
    matrix-matrix multiplication C = A·B where A in Zm×p, B in Zp×n, C in Zm×n (cij = Σk=1, ..., p aik·bkj for i = 1,...,m j = 1,...,n);
    matrix transposition B = AT where A in Zm×n, B in Zn×m (bji = aij for i = 1,...,m j = 1,...,n);
    matrix norm (matrix 1-norm) a = ‖A‖ where a in Z, A in Zm×n (a = maxj Σi | aij | for i = 1,...,m j = 1,...,n).

Note that in each case we state the proper matrix dimensions for the operation to be valid. For example, when multiplying two matrices A and B, the number of columns of A must match the number of rows of B.