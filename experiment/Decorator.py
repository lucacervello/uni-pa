from functools import wraps


def this_dec(func):

    @wraps(func)
    def wrapper(*args, **kwargs):
        print("Decorato")
        return func(*args, **kwargs)
    return wrapper


@this_dec
def decorated_func():
    return 3


if __name__ == '__main__':
    decorated_func()