
class MetaLog(type):

    def __new__(cls, clsname, bases, clsdict):
        res = super().__new__(cls, clsname, bases, clsdict)
        print("object created")
        return res


class LogTest(metaclass=MetaLog):
    def __init__(self):
        self.hello = "hello"

    def print(self):
        print(self.hello)
