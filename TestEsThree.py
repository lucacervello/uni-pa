import unittest
from ex3 import Geometry as G, SortedDict as SD


class GeometryTest(unittest.TestCase):

    def test_list_shape(self):
        self.assertEqual([20, 22], [x.calculate_perimeter() for x in [G.Square(5), G.Rectangle(5, 6)]])

    def test_sort_list(self):
        rectangle = G.Rectangle(5, 6)
        square = G.Square(5)
        lista = [rectangle, square]
        self.assertEquals([square, rectangle], sorted(lista, key=lambda x: x.calculate_perimeter()))

    def test_sort_list_area(self):
        rectangle = G.Rectangle(5, 6)
        square = G.Square(5)
        lista = [rectangle, square]
        self.assertEquals([square, rectangle], sorted(lista, key=lambda x: x.calculate_area()))


class SortedDictTest(unittest.TestCase):

    def setUp(self):
        self.d = SD.SortedDict()
        self.d["a"] = 1
        self.d["c"] = 3
        self.d["b"] = 2

    def test_sort_keys(self):
        self.assertEquals(["a", "b", "c"], self.d.keys)

    def test_sort_value(self):
        self.assertEquals([1, 2, 3], [v for (k, v) in self.d])

    def test_clear(self):
        self.d.clear()
        self.assertEquals({}, self.d)


if __name__ == '__main__':
    unittest.main()