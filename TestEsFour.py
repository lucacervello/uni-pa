import unittest
from ex4 import ExtendedString as ES


class ExtendedStringTest(unittest.TestCase):

    def test_palindrome(self):
        self.assertTrue(ES.ExtendedString("detartrated").is_palindrome())
        self.assertTrue(ES.ExtendedString("Do geese see God?").is_palindrome())
        self.assertTrue(ES.ExtendedString("Rise to vote, sir.").is_palindrome())

    def test_minus(self):
        self.assertEquals(ES.ExtendedString("Walter Cazzola") - ES.ExtendedString("abcwxyz"), ES.ExtendedString("Wlter Col"))

if __name__ == '__main__':
    unittest.main()